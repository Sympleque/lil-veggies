
using Mirror;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(FirstPersonInput), typeof(CharacterController))]
public class FirstPersonController : NetworkBehaviour
{
    [SerializeField]
	private float _playerSpeed = 2.0f;

	[SerializeField]
	private float _jumpHeight = 1.0f;

	[SerializeField]
	private GameObject _head;

	[Header("Setup")]
	[SerializeField]
	[Tooltip("This will only be instantiated for the local player.")]
	private CinemachineVirtualCamera _pfVirutalCamera;

	private CharacterController _controller;

	private FirstPersonInput _input;

	private Vector3 _playerVelocity;

	private Transform _cameraTransform;
	
    private bool _groundedPlayer;

	public override void OnStartLocalPlayer()
	{
		_cameraTransform = Camera.main.transform;
		_controller = GetComponent<CharacterController>();
		_input = GetComponent<FirstPersonInput>();

		_input.enabled = true;
		CinemachineVirtualCamera cam = Instantiate(_pfVirutalCamera);
		cam.Follow = _head.transform;
	}

	private void Update()
	{
        if (isLocalPlayer)
			HandleInput();
	}

	[Client]
	private void HandleInput()
	{
		transform.rotation = Quaternion.Euler(0.0f, _cameraTransform.eulerAngles.y, 0.0f);

		_groundedPlayer = _controller.isGrounded;
		if (_groundedPlayer && _playerVelocity.y < 0)
			_playerVelocity.y = 0f;

		Vector3 move = transform.forward * _input.Move.y + transform.right * _input.Move.x;

		move = move.normalized;
		move.y = 0.0f;

		_controller.Move(move * Time.deltaTime * _playerSpeed);

		if (_input.Jump && _groundedPlayer)
			_playerVelocity.y += Mathf.Sqrt(_jumpHeight * -3.0f * Physics.gravity.y);

		_playerVelocity.y += Physics.gravity.y * Time.deltaTime;
		_controller.Move(_playerVelocity * Time.deltaTime);
	}

	private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
	{
		if (lfAngle < -360f) lfAngle += 360f;
		if (lfAngle > 360f) lfAngle -= 360f;
		return Mathf.Clamp(lfAngle, lfMin, lfMax);
	}
}