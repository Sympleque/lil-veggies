
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
public class FirstPersonInput : MonoBehaviour
{
	public Vector2 Move{ get; private set; }

	public Vector2 Look{ get; private set; }
	
	public bool Jump{ get; private set; }

	private PlayerInput _input;

	private void Awake()
	{
		_input = GetComponent<PlayerInput>();
	}

	public void OnMovement(InputValue value)
	{
		Move = value.Get<Vector2>();
	}

	public void OnLook(InputValue value)
	{
		Look = value.Get<Vector2>();
	}

	public void OnJump(InputValue value)
	{
		Jump = value.isPressed;
	}

    private void OnEnable()
    {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		_input.enabled = true;
    }

    private void OnDisable()
    {
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		_input.enabled = false;
	}
}
